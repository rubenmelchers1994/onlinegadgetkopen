<?php 
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $image = get_sub_field('section_image');
    $rows = get_sub_field('table_rows')
?>

<section class="section table">
    <div class="section__inner">

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2 justify-content-md-center">
    
                    <?php if($title) : ?>
                        <div class="section__title text-center">
                            <h2><?= $title ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if($content) : ?>
                        <div class="section__contentWrapper">
                            <?= $content ?>
                        </div>
                    <?php endif; ?>
    
                </div>

                <?php if($rows): ?>
                    <div class="col-12 col-md-8 offset-md-2 justify-content-md-center">

                        <div class="table__wrapper">
                            <?php foreach($rows as $row): ?>
                                <?php 
                                    $label = $row['label'];
                                    $value = $row['value'];
                                ?>
                                <div class="table__row d-flex">
                                    <div class="table__label w-50"><?= $label ?></div>
                                    <div class="table__value w-50"><?= $value ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>