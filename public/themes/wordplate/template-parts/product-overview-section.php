<?php 
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $image = get_sub_field('section_image');
    $products = get_sub_field('products');
    $button = get_sub_field('button');
?>

<section class="section productOverview">
    <div class="section__inner">

        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">
    
                    <?php if($title) : ?>
                        <div class="section__title">
                            <h2><?= $title ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if($content) : ?>
                        <div class="section__contentWrapper">
                            <?= $content ?>
                        </div>
                    <?php endif; ?>
    
                </div>
                <?php if($products): ?>
                    <div class="col-12 mt-5">
                        <div class="row section__overview">
                            <?php foreach($products as $product): ?>
                                <div class="col-6 col-lg-3">
                                    <?php get_template_part('template-parts/product-card', null, array(
                                        'product' => wc_get_product( $product->ID )
                                    )); ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if($button): ?>
                    <div class="col-12">
                        <div class="section__buttonsWrapper d-flex justify-content-center mt-5">
                            <a href="<?= esc_url($button['url']) ?>" class="button button--cta button--blue section__button">
                                <span class="section__label">
                                    <?= $button['title'] ?>
                                </span>
                                <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-right-white.svg" alt="<?= $link['link']['title'] ?>" class="button__icon">
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>