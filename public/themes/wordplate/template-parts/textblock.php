<?php 
    $title = get_sub_field('title');
    $id = get_sub_field('id');
    $content = get_sub_field('content');
?>

<?php if($title || $content) : ?>
    <section class="section textblock position-relative">
        <?php if($id): ?>
            <div class="visually-hidden" style="top:-50px;" id="<?= $id ?>"></div>
        <?php endif; ?>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-11 col-md-10">
                    <?php if($title): ?>
                        <div class="textblock__title">
                            <h2><?= $title ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if($content): ?>
                        <div class="textblock__contentWrapper">
                            <?= $content ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>