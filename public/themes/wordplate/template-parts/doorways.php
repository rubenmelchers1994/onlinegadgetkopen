<?php 
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $bgColor = get_sub_field('background_color')?: 'white';
    $image = get_sub_field('section_image');
    $links = get_sub_field('links');
?>

<section class="section section--<?= $bgColor ?> doorways">
    <div class="section__inner">

        <div class="container">
            <div class="row justify-content-md-between">
                <div class="col-12 col-lg-5">
    
                    <?php if($title) : ?>
                        <div class="section__title">
                            <h2><?= $title ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if($content) : ?>
                        <div class="section__contentWrapper">
                            <?= $content ?>
                        </div>
                    <?php endif; ?>
    
                    <?php if($links): ?>
                        <div class="doorways__linksWrapper">
                            <ul class="doorways__list list-unstyled mb-0 d-flex flex-column">
                                <?php foreach($links as $link): ?>
                                    <li class="doorways__listItem d-flex">
                                        <a href="<?= esc_url($link['link']['url']) ?>" class="doorways__link d-flex justify-content-between" target="<?= $link['link']['target'] ? $link['link']['target'] : '_self' ?>">
                                            <span class="doorways__label">
                                                <?= esc_html($link['link']['title']) ?>
                                            </span>    
                                            <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-right-white.svg" alt="<?= $link['link']['title'] ?>" class="doorways__icon">
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if($image): ?>
                    <div class="col-12 col-lg-6">
                        <div class="section__imageWrapper d-flex justify-content-center mt-5 mt-lg-0">
                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="section__image img-fluid">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>