<?php 
    $title = get_sub_field('title');
    $content = get_sub_field('content');
    $bgColor = get_sub_field('background_color')?: 'white';
    $image = get_sub_field('section_image');
    $questions = get_sub_field('faqs');
    $id = get_sub_field('id');
?>

<section class="section section--<?= $bgColor ?> faq">
    <?php if($id): ?>
        <div class="visually-hidden" style="top:-50px;" id="<?= $id ?>"></div>
    <?php endif; ?>
    <div class="section__inner">

        <div class="container">
            <div class="row justify-content-md-between">
                <div class="col-12 col-lg-5">
    
                    <?php if($title) : ?>
                        <div class="section__title">
                            <h2><?= $title ?></h2>
                        </div>
                    <?php endif; ?>
                    <?php if($content) : ?>
                        <div class="section__contentWrapper">
                            <?= $content ?>
                        </div>
                    <?php endif; ?>
    
                    <?php if($questions): ?>
                        <div class="accordion accordion-flush faq mt-4">
                            <?php foreach($questions as $key=>$faq): ?>
                                <?php $id = "faq-".get_row_index()."_".$key ?>
                                <div class="accordion-item faq__item">
                                    <h2 class="accordion-header" id="<?= $id ?>">
                                        <button class="accordion-button faq__question collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#q-<?= $id ?>" aria-expanded="false" aria-controls="q-<?= $id ?>">
                                            <?= $faq['question'] ?>
                                        </button>
                                    </h2>
                                    <div id="q-<?= $id ?>" class="accordion-collapse faq__answer collapse" aria-labelledby="<?= $id ?>" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <?= $faq['answer'] ?>
                                    </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if($image): ?>
                    <div class="col-12 col-lg-6">
                        <div class="section__imageWrapper d-flex justify-content-center mt-5 mt-lg-0">
                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="section__image img-fluid">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>