<?php 
    $locations = get_nav_menu_locations();
    // $items = wp_get_nav_menu_items('navigation');
    // $menu = wp_get_nav_menu_object( $locations[ 'navigation' ] );
    // $menu_items = wp_get_nav_menu_items($menu->term_id);

    $nav_menu = navigation_menu('navigation');

    $logo = get_field('site_logo', 'options');
?>
<nav role="navigation" class="navigation" data-js-bind="navigation">
    <div class="container">
        <div class="row">
            <?php if($logo): ?>
                <div class="col-8 col-md-3">
                    <a href="<?= home_url() ?>" class="navigation__link">
                        <img src="<?= $logo['url'] ?>" alt="onlinegadgetkopen" class="navigation__logo">
                    </a>
                </div>
            <?php endif; ?>
            <div class="col-4 d-flex d-md-none justify-content-end align-items-center">
                <div class="navigation__hamburger position-relative" data-js-bind="hamburger">
                    <div class="navigation__bar navigation__bar--top"></div>
                    <div class="navigation__bar navigation__bar--mid"></div>
                    <div class="navigation__bar navigation__bar--bot"></div>
                </div>
            </div>

            <div class="col-12 col-md-8 col-lg-6">
                <?= $nav_menu ?>
            </div>

            <div class="col-12 col-md-3 d-none d-lg-block">
                <?= get_search_form() ?>
            </div>
        </div>
    </div>
</nav>