<?php 
    $product = $args['product']?: false;
?>
<?php if($product): ?>
    <div class="productCard">
        <div class="productCard__imageWrapper position-relative">
            <?= $product->get_image() ?>
            <a href="<?= get_permalink($product->get_id()) ?>" class="position-absolute w-100 h-100" title="<?= $product->get_name(); ?>" aria-label="<?= $product->get_name(); ?>"></a>
        </div>
        <div class="productCard__title">
            <?= $product->get_name(); ?>
        </div>
        <div class="productCard__price">
            €<?= $product->get_price() ?>
        </div>
        <div class="productCard__links d-flex align-items-center flex-column flex-lg-row">
            <a href="<?= $product->get_product_url() ?>" class="text-decoration-none productCard__link productCard__link--cta text-center" rel="nofollow noreferrer" target="_blank">
                Bezoek site
            </a>
            
            <a href="<?= get_permalink($product->get_id()) ?>" class="productCard__link ">Lees meer</a>
        </div>
    </div>
<?php endif; ?>