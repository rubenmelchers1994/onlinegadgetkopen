<?php

add_action('after_setup_theme', function () {
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    register_nav_menus([
        'navigation' => __('Navigation'),
    ]);
});

include "app/acf/acf.php";
// include "app/functions/*.php";

$configPathLogic = get_template_directory() . "/app/functions/*.php";
$configPathMatches = glob($configPathLogic);

foreach($configPathMatches as $configPath){
    include $configPath;
}