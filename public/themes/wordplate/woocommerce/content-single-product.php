<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$anchors = get_field('page_anchors');
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'productDetail', $product ); ?>>
    <div class="productDetail__top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5 order-2 order-lg-1">
                    <div class="productDetail__leftColWrapper d-flex justify-content-end">
                        <?php
                            /**
                             * Hook: woocommerce_before_single_product_summary.
                             *
                             * @hooked woocommerce_show_product_sale_flash - 10
                             * @hooked woocommerce_show_product_images - 20
                             */
                            do_action( 'woocommerce_before_single_product_summary' );
                        ?>
                    </div>
                </div>
    
                <div class="col-12 col-lg-7 order-1 order-lg-2">
    
                    <div class="productDetail__info">
                        <?php
                        /**
                         * Hook: woocommerce_single_product_summary.
                         *
                         * @hooked woocommerce_template_single_title - 5
                         * @hooked woocommerce_template_single_rating - 10
                         * @hooked woocommerce_template_single_price - 10
                         * @hooked woocommerce_template_single_excerpt - 20
                         * @hooked woocommerce_template_single_add_to_cart - 30
                         * @hooked woocommerce_template_single_meta - 40
                         * @hooked woocommerce_template_single_sharing - 50
                         * @hooked WC_Structured_Data::generate_product_data() - 60
                         */
                        do_action( 'woocommerce_single_product_summary' );
                        ?>

                        <?php if($anchors): ?>
                            <div class="productDetail__anchorsWrapper">
                                <ul class="productDetail__anchorsList list-unstyled d-flex flex-wrap gap-3">
                                    <?php foreach($anchors as $anchor): ?>
                                        <?php 
                                            $anchorId = $anchor['anchor_id'];
                                            $anchorLabel = $anchor['anchor_label'];
                                        ?>
                                        <?php if($anchorId && $anchorLabel): ?>
                                            <li class="productDetail__anchorItem d-flex">
                                                <a href="#<?= $anchorId ?>" class="productDetail__anchorLink text-center" data-js-bind="anchor">
                                                    <?= $anchorLabel ?>
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
    
                        <?php if(get_field('header_content')): ?>
                            <div class="productDetail__intro">
                                <div class="productDetail__accordion">
                                    <h2 class="productDetail__heading heading accordion-header" id="<?php the_ID() ?>">
                                        <button class="accordion-button productDetail__accordionButton" type="button" data-bs-toggle="collapse" data-bs-target="#q-<?php the_ID() ?>" aria-expanded="true" aria-controls="q-<?php the_ID() ?>">
                                            Beschrijving
                                        </button>
                                    </h2>
                                    <div id="q-<?php the_ID() ?>" class="accordion-collapse productDetail__introInner collapse show" aria-labelledby="<?php the_ID() ?>">
                                    <div class="accordion-body">
                                        <?= get_field('header_content') ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
    
                </div>
            </div>
    
        </div>
    </div>
    
    <?php 
        if ( have_rows( 'flexible_content')) :
            while ( have_rows( 'flexible_content' ) ) : the_row();
                switch(get_row_layout()):
                    case 'textblock':
                        get_template_part( 'template-parts/textblock' );
                        break;
                    case 'faq':
                        get_template_part( 'template-parts/faq' );
                        break;
                    case 'table':
                        get_template_part( 'template-parts/table' );
                        break;
                    case 'product_overview':
                        get_template_part( 'template-parts/product-overview-section' );
                        break;
                    case 'text_and_image':
                        get_template_part( 'template-parts/text-and-image' );
                        break;
                    default:
                        break;
                endswitch;
            endwhile; 
        endif; 
    ?>
    <div class="container">
        <div class="row">
            <div class="col-12">

                <?php
                    /**
                     * Hook: woocommerce_after_single_product_summary.
                     *
                     * @hooked woocommerce_output_product_data_tabs - 10
                     * @hooked woocommerce_upsell_display - 15
                     * @hooked woocommerce_output_related_products - 20
                     */
                    do_action( 'woocommerce_after_single_product_summary' );
                ?>
            </div>
        </div>
    </div>
	
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>