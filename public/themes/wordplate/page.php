<?php get_header(); ?>

<?php 
    $image = get_field('header_image');
    $content = get_field('header_content');
    $button = get_field('header_button');
?>
<main role="main">
    
    <div class="banner banner--home">
        <div class="container">
            <div class="row">
                <div class="col-9 col-md-6">
                    <div class="banner__contentWrapper banner__contentWrapper--content">
                        <h1 class="banner__title">
                            <?= get_the_title() ?>
                        </h1>
                        <?= $content ?>

                        <?php if($button): ?>
                            <a href="<?= esc_url($button['url']) ?>" class="button button--banner button--orangeCta d-inline-flex justify-content-between mt-4" target="<?= $button['target'] ? $button['target'] : '_self' ?>">
                                <span class="button__label">
                                    <?= esc_html($button['title']) ?>
                                </span>    
                                <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-right-white.svg" alt="<?= $button['title'] ?>" class="button__icon">
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if($image): ?>
                    <div class="col-12 col-md-6 mt-3 mt-md-0 d-none d-md-flex">
                        <div class="banner__imageWrapper d-flex justify-content-end">
                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="banner__image img-fluid">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php 
        if ( have_rows( 'flexible_content')) :
            while ( have_rows( 'flexible_content' ) ) : the_row();
                switch(get_row_layout()):
                    case 'textblock':
                        get_template_part( 'template-parts/textblock' );
                        break;
                    case 'doorways':
                        get_template_part( 'template-parts/doorways' );
                        break;
                    case 'faq':
                        get_template_part( 'template-parts/faq' );
                        break;
                    case 'contact_form':
                        get_template_part( 'template-parts/contact-form' );
                        break;
                    case 'product_overview':
                        get_template_part( 'template-parts/product-overview-section' );
                        break;
                    case 'text_and_image':
                        get_template_part( 'template-parts/text-and-image' );
                        break;
                    default:
                        break;
                endswitch;
            endwhile; 
        endif; 
    ?>
    
    
</main>

<?php get_footer(); ?>
