<?php get_header(); ?>
<?php 
    $image = get_field('header_image');
?>
<main role="main">
    <?php if(is_shop()): ?>
        
        <div class="banner banner--home">
            <div class="container">
                <div class="row">
                    <div class="col-9 col-md-6">
                        <div class="banner__contentWrapper">
                            <h1 class="banner__title">
                                <?= get_field('page_title')?: get_the_title() ?>
                            </h1>
                        </div>
                    </div>
                    <?php if($image): ?>
                        <div class="col-12 col-md-6 mt-3 mt-md-0 d-none d-md-flex">
                            <div class="banner__imageWrapper d-flex justify-content-end">
                                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="banner__image img-fluid">
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <section class="overview">
            <div class="container">
                <div class="row">
                    
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <article>
                        <header>
                            <h1><?php the_title(); ?></h1>
                        </header>
            
                        <?php the_content(); ?>
                    </article>
                <?php endwhile; endif; ?>
                </div>
            </div>
        </section>

        <?php else :?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <article>
                <header>
                    <h1><?php the_title(); ?></h1>
                </header>
    
                <?php the_content(); ?>
            </article>
        <?php endwhile; endif; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>
