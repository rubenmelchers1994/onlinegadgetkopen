<?php get_header(); ?>

<?php 
    $image = get_field('header_image');
    $featuredProducts = get_field('products');
?>
<main role="main">
    
    <div class="banner banner--home">
        <div class="container">
            <div class="row">
                <div class="col-9 col-md-6">
                    <div class="banner__contentWrapper">
                        <h1 class="banner__title">
                            Vind de beste online gadget deals
                        </h1>
                        <div class="banner__searchWrapper d-flex mt-4">
                            <?= get_search_form() ?>
                        </div>
                    </div>
                </div>
                <?php if($image): ?>
                    <div class="col-12 col-md-6 mt-3 mt-md-0 d-none d-md-flex">
                        <div class="banner__imageWrapper d-flex justify-content-end">
                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="banner__image img-fluid">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if($featuredProducts && count($featuredProducts) > 0 ): ?>
        <section class="featuredProductsSlider">
            <div class="container">
                <?php if(count($featuredProducts) <= 3) : ?>
                    <div class="row justify-content-center">
                        <div class="col-1"></div>
                        <div class="col-8">
                            <div class="featuredProductsSlider__wrapper">
                                <div class="row">
                                    <?php foreach($featuredProducts as $product): ?>
                                        <div class="col-4">
                                            <?php get_template_part('template-parts/product-card', null, array(
                                                'product' => wc_get_product( $product->ID )
                                            )); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-1"></div>
                    </div>
                <?php endif;?>

                <?php if(count($featuredProducts) > 3) : ?>
                    <div data-js-bind="slider-element">
                        <div class="row justify-content-center glide">
                            <div class="d-flex align-items-center featuredProductsSlider__buttonWrapper col-1 justify-content-end" data-glide-el="controls">
                                <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-left-dark.svg" alt="Arrow pointing left">
                                </button>
                            </div>
                            <div class="col-10 col-md-8">
                                <div class="featuredProductsSlider__wrapper">
                                    <div class="glide__track" data-glide-el="track">
                                        <div class="glide__slides">
                                            <?php foreach($featuredProducts as $product): ?>
                                                <div class="glide__slide">
                                                    <?php get_template_part('template-parts/product-card', null, array(
                                                        'product' => wc_get_product( $product->ID )
                                                    )); ?>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center featuredProductsSlider__buttonWrapper col-1" data-glide-el="controls">
                                <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                                    <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-right-dark.svg" alt="Arrow pointing left">
                                </button>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endif; ?>

    <?php 
        if ( have_rows( 'flexible_content')) :
            while ( have_rows( 'flexible_content' ) ) : the_row();
                switch(get_row_layout()):
                    case 'textblock':
                        get_template_part( 'template-parts/textblock' );
                        break;
                    case 'doorways':
                        get_template_part( 'template-parts/doorways' );
                        break;
                    case 'faq':
                        get_template_part( 'template-parts/faq' );
                        break;
                    case 'contact_form':
                        get_template_part( 'template-parts/contact-form' );
                        break;
                    case 'product_overview':
                        get_template_part( 'template-parts/product-overview-section' );
                        break;
                    case 'text_and_image':
                        get_template_part( 'template-parts/text-and-image' );
                        break;
                    default:
                        break;
                endswitch;
            endwhile; 
        endif; 
    ?>
    
    
</main>

<?php get_footer(); ?>
