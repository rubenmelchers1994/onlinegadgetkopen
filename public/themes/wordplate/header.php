<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php if(get_field('enable_cookies', 'options')): ?>
    <script async id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="c83f26f5-a478-442a-b5e8-9d5f726c5dc0" data-blockingmode="auto" type="text/javascript"></script>
  <?php endif; ?>

  <meta http-equiv="x-dns-prefetch-control" content="on">
  <link rel="preconnect" href="https://www.google-analytics.com" crossorigin>
  <link rel="preconnect" href="https://www.googletagmanager.com" crossorigin>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  
  <link rel="preload" href="https://fonts.gstatic.com/s/overpass/v7/qFdH35WCmI96Ajtm81GlU9vgwBcI.woff2" crossorigin="anonymous" as="font" type="font/woff2">
  <link rel="preload" href="https://fonts.gstatic.com/s/heebo/v15/NGS6v5_NC0k9P9H2TbFhsqMA.woff2" crossorigin="anonymous" as="font" type="font/woff2">
  <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600;700;800;900&family=Overpass:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

  <?php if(get_field('enable_analytics', 'options')) : ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TL0CC7HHD3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-TL0CC7HHD3');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TP9VXT3');</script>
    <!-- End Google Tag Manager -->
  <?php endif; ?>

  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head(); ?>

  <link rel="stylesheet" href="<?= get_theme_file_uri('assets/css/app.css') ?>">
  <script defer src="<?= get_theme_file_uri('assets/js/manifest.js') ?>"></script>
  <script defer src="<?= get_theme_file_uri('assets/js/vendor.js') ?>"></script>
  <script defer src="<?= get_theme_file_uri('assets/js/app.js') ?>"></script>

  <link rel="apple-touch-icon" sizes="180x180" href="<?= get_theme_file_uri('apple-touch-icon.png') ?>">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= get_theme_file_uri('favicon-32x32.png') ?>">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= get_theme_file_uri('favicon-16x16.png') ?>">
  <link rel="manifest" href="<?= get_theme_file_uri('site.webmanifest') ?>">
  <link rel="mask-icon" href="<?= get_theme_file_uri('safari-pinned-tab.svg" color="#5bbad5') ?>">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

</head>
<body <?php body_class(); ?>>
  <?php wp_body_open(); ?>
  <?php if(get_field('enable_analytics', 'options')) : ?>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TP9VXT3"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
  <?php endif; ?>

    <?php get_template_part('template-parts/navigation') ?>
