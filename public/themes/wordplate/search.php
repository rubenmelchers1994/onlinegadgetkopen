<?php
/*
Template Name: Search Page
*/
?>
<?php
get_header(); ?>

<!-- Header -->
<div class="header banner banner--home" style="margin-bottom: 15px;">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="banner__title">Zoekresultaten</h1>
      </div>
      <div class="col-12 col-md-5">
      <div class="banner__searchWrapper d-flex mt-4">
      <?= get_search_form() ?>
      </div>
      </div>
    </div>
  </div>
</div>

<section class="section searchResults">
  <div class="container">
    <div class="row">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
        <?php if(wc_get_product( get_the_ID() )) : ?>
          <div class="col-6 col-md-4">
            <?php get_template_part('template-parts/product-card', null, array(
                'product' => wc_get_product( get_the_ID() )
            )); ?>
          </div>
        <?php endif; ?>
      <?php endwhile; ?>
      <?php else : ?>
        <div class="searchResults__heading">
          Geen resultaten gevonden
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>

<!-- Pagination -->
<div class="container margin__bottom">
  <div class="row">
    <div class="pagination">
      <?php if (function_exists("pagination")) {
          pagination($additional_loop->max_num_pages);
      } ?>
    </div>
  </div>
</div>

<?php get_footer();