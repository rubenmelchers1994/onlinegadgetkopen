<?php get_header(); ?>
<?php 
    $image = get_field('header_image');
?>
<main role="main">
    <div class="banner banner--home">
        <div class="container">
            <div class="row">
                <div class="col-9 col-md-6">
                    <div class="banner__contentWrapper">
                        <h1 class="banner__title">
                            <?= get_field('page_title')?: get_the_title() ?>
                        </h1>
                    </div>
                </div>
                <?php if($image): ?>
                    <div class="col-12 col-md-6 mt-3 mt-md-0 d-none d-md-flex">
                        <div class="banner__imageWrapper d-flex justify-content-end">
                            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="banner__image img-fluid">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <section class="overview">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="overview__filtersHolder d-flex flex-column" data-js-bind="filter-holder">
                        <div class="overview__filterButton d-flex d-md-none text-center button justify-content-center py-2 mb-3" data-js-bind="filters-trigger">Filters</div>
                        <div class="overview__filtersOuter" data-js-bind="filters-outer">
                            <div class="overview__closeButton d-flex justify-content-end d-md-none" data-js-bind="filters-close-button">
                                <img src="<?= get_template_directory_uri() ?>/assets/img/cross-white.svg" alt="Sluit filters" width="20" height="20">
                            </div>
                            <div class="overview__filterWrapper overview__filterWrapper--search search">
                                <?php echo do_shortcode('[facetwp facet="product_search"]') ?>
                            </div>
                            <div class="overview__filterWrapper overview__filterWrapper--checkboxes">
                                <div class="accordion-item overview__item">
                                    <h2 class="accordion-header" id="acc-cats">
                                        <button class="accordion-button overview__filterHeading collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#q-acc-cats" aria-expanded="true" aria-controls="q-acc-cats">
                                            Categorieën
                                        </button>
                                    </h2>
                                    <div id="q-acc-cats" class="accordion-collapse overview__answer collapse show" aria-labelledby="acc-cats">
                                    <div class="accordion-body">
                                        <?php echo do_shortcode('[facetwp facet="prod_categories"]') ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overview__filterWrapper overview__filterWrapper--checkboxes">
                                <div class="accordion-item overview__item">
                                    <h2 class="accordion-header" id="acc-seller">
                                        <button class="accordion-button overview__filterHeading collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#q-acc-seller" aria-expanded="true" aria-controls="q-acc-seller">
                                            Verkoper
                                        </button>
                                    </h2>
                                    <div id="q-acc-seller" class="accordion-collapse overview__answer collapse show" aria-labelledby="acc-seller">
                                    <div class="accordion-body">
                                        <?php echo do_shortcode('[facetwp facet="product_seller"]') ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overview__filterWrapper overview__filterWrapper--checkboxes">
                                <div class="accordion-item overview__item">
                                    <h2 class="accordion-header" id="acc-theme">
                                        <button class="accordion-button overview__filterHeading collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#q-acc-theme" aria-expanded="true" aria-controls="q-acc-theme">
                                            Thema
                                        </button>
                                    </h2>
                                    <div id="q-acc-theme" class="accordion-collapse overview__answer collapse show" aria-labelledby="acc-theme">
                                    <div class="accordion-body">
                                        <?php echo do_shortcode('[facetwp facet="product_theme"]') ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overview__filterWrapper overview__filterWrapper--slider">
                                <?php echo do_shortcode('[facetwp facet="price_slider"]') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8 col-lg-9">
                    <?php echo do_shortcode('[facetwp template="products"]') ?>
                </div>
                    
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>
