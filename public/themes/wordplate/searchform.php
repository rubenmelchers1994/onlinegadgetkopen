<?php $id = uniqid('search_') ?>
<form action="/" method="get" class="search h-100">
    <div class="search__wrapper h-100 d-flex justify-content-between">
        <label for="<?= $id ?>" class="visually-hidden">Zoek binnen onlinegadgetkopen.nl</label>
        <input class="search__input" type="text" name="s" id="<?= $id ?>" placeholder="Stofzuiger..." value="<?php the_search_query(); ?>" />
        <input class="search__submit" type="image" alt="Search" src="<?= get_template_directory_uri() ?>/assets/img/icon-search.svg" />
    </div>
</form>