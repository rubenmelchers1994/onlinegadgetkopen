<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$options = new FieldsBuilder('product_options', [
    'menu_order' => 10,
    'position' => 'side'
]);
$options
    ->addCheckbox('product_seller', [
        'label' => 'Product seller',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'choices' => [
            0 => 'Bol.com',
            1 => 'Coolblue',
            2 => 'Amazon',
            3 => 'Aliexpres',
            4 => 'Banggood',
        ],
        'allow_custom' => 1,
        'save_custom' => 1,
        'default_value' => [],
        'layout' => 'vertical',
        'toggle' => 0,
        'return_format' => 'value',
    ])
    ->addCheckbox('product_theme', [
        'label' => 'Product theme',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'choices' => [
            0 => 'Kerst',
            1 => 'Sinterklaas',
            2 => 'Verjaardag',
            3 => 'Mannen',
            4 => 'Honden',
        ],
        'allow_custom' => 1,
        'save_custom' => 1,
        'default_value' => [],
        'layout' => 'vertical',
        'toggle' => 0,
        'return_format' => 'value',
    ])
    ->setLocation('post_type', '==', 'product');


return $options;