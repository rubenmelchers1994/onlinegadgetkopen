<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$header = new FieldsBuilder('header_default', [
    'position' => 'acf_after_title',
    'menu_order' => 1
]);
$header
    ->addText('page_title')
    ->addImage('header_image')
    ->addWysiwyg('header_content')
    ->addLink('header_button')
    ->setLocation('page_type', '!=', 'front_page');

    return $header;
// add_action('acf/init', function() use ($default) {
//    acf_add_local_field_group($default->build());
// });