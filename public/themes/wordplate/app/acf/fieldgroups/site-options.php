<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$options = new FieldsBuilder('options', [
    'position' => 'acf_after_title',
    'menu_order' => 1
]);
$options
    ->addImage('site_logo', [
        'label' => 'Site logo'
    ])
    ->addTrueFalse('enable_analytics', [
        'label' => 'Enable analytics',
        'ui' => 1,
        'wrapper' => [
            'width' => '50%'
        ]
    ])
    ->addTrueFalse('enable_cookies', [
        'label' => 'Enable cookies',
        'ui' => 1,
        'default_value' => 1,
        'wrapper' => [
            'width' => '50%'
        ]
    ])
    ->setLocation('options_page', '==', 'site-options');


return $options;