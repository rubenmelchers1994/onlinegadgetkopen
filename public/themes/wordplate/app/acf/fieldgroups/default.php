<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$textblock = include get_template_directory() . "/app/acf/molecules/textblock.php";
$doorways = include get_template_directory() . "/app/acf/molecules/doorways.php";
$productOverviewSection = include get_template_directory() . "/app/acf/molecules/product-overview-section.php";
$faq = include get_template_directory() . "/app/acf/molecules/faq.php";
$contactForm = include get_template_directory() . "/app/acf/molecules/contact-form.php";
$textAndImage = include get_template_directory() . "/app/acf/molecules/text-and-image.php";

$default = new FieldsBuilder('default', [
    'position' => 'acf_after_title',
    'menu_order' => 2
]);
$default
    ->setLocation('post_type', '==', 'post')
        ->or('post_type', '==', 'page');

$default
    ->addFlexibleContent('flexible_content', [
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
        'width' => '',
        'class' => '',
        'id' => '',
        ],
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
    ])
        ->addLayout('textblock', [
            'label' => 'Textblock',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($textblock)
        ->addLayout('doorways', [
            'label' => 'Doorways',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($doorways)
        ->addLayout('product_overview', [
            'label' => 'Product overview section',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($productOverviewSection)
        ->addLayout('faq', [
            'label' => 'Frequently asked questions',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($faq)
        ->addLayout('text_and_image', [
            'label' => 'Text and image',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($textAndImage)
        ->addLayout('contact_form', [
            'label' => 'Contact form',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($contactForm);

return $default;
// add_action('acf/init', function() use ($default) {
//    acf_add_local_field_group($default->build());
// });