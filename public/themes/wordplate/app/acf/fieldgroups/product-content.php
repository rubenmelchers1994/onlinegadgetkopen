<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$textblock = include get_template_directory() . "/app/acf/molecules/textblock.php";
$productOverviewSection = include get_template_directory() . "/app/acf/molecules/product-overview-section.php";
$faq = include get_template_directory() . "/app/acf/molecules/faq.php";
$table = include get_template_directory() . "/app/acf/molecules/table.php";
$textAndImage = include get_template_directory() . "/app/acf/molecules/text-and-image.php";

$productContent = new FieldsBuilder('product_content', [
    'position' => 'acf_after_title',
    'menu_order' => 2
]);
$productContent
    ->setLocation('post_type', '==', 'product');

$productContent
    ->addRepeater('page_anchors', [
            'label' => 'anchors'
        ])
        ->addText('anchor_id', [
            'label' => 'section id',
            'prepend' => '#',
            'wrapper' => [
                'width' => '30%'
            ]
        ])
        ->addText('anchor_label', [
            'label' => 'anchor label',
            'wrapper' => [
                'width' => '70%'
            ]
        ])
    ->endRepeater()
    ->addFlexibleContent('flexible_content', [
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
        'width' => '',
        'class' => '',
        'id' => '',
        ],
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
    ])
        ->addLayout('textblock', [
            'label' => 'Textblock',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($textblock)
        ->addLayout('product_overview', [
            'label' => 'Product overview section',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($productOverviewSection)
        ->addLayout('faq', [
            'label' => 'Frequently asked questions',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($faq)
        ->addLayout('text_and_image', [
            'label' => 'Text and image',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($textAndImage)
        ->addLayout('table', [
            'label' => 'Table',
            'display' => 'block',
            'min' => '',
            'max' => '',
        ])
        ->addFields($table);

return $productContent;