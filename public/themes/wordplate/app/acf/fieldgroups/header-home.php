<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$header = new FieldsBuilder('header_home', [
    'position' => 'acf_after_title',
    'menu_order' => 1
]);
$header
    ->addTab('Page settings')
        ->addText('page_title')
        ->addImage('header_image')
    ->addTab('Featured products')
        ->addRelationship('products', [
            'label' => 'Products',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['product'],
            'taxonomy' => [],
            'filters' => [
                0 => 'search',
                2 => 'taxonomy',
            ],
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'object',
        ])
    ->setLocation('page_type', '==', 'front_page');

return $header;
// add_action('acf/init', function() use ($default) {
//    acf_add_local_field_group($default->build());
// });