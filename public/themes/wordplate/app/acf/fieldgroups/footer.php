<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$footer = new FieldsBuilder('footer', [
    'menu_order' => 10
]);
$footer
    ->addTab('First column')
        ->addText('slogan')
    ->addTab('Second column')
        ->addRelationship('page_links', [
            'label' => 'Page Links',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['page', 'post', 'product'],
            'taxonomy' => [],
            'filters' => [
                0 => 'search',
                2 => 'taxonomy',
            ],
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'object',
        ])
    ->addTab('Third column')
        ->addRelationship('secondary_page_links', [
            'label' => 'Page Links',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['page', 'post', 'product'],
            'taxonomy' => [],
            'filters' => [
                0 => 'search',
                2 => 'taxonomy',
            ],
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'object',
        ])
    ->setLocation('options_page', '==', 'site-options');


return $footer;