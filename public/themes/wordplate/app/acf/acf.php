<?php

// include "fieldgroups/home.php";
// include "fieldgroups/default.php";



$fieldgroupPath = get_template_directory() . '/app/acf/fieldgroups/*.php';
$fieldgroupPaths = glob($fieldgroupPath);

foreach($fieldgroupPaths as $fieldgroupFile){
    $fieldgroup = include $fieldgroupFile;
    add_action('acf/init', function() use ($fieldgroup) {
        acf_add_local_field_group($fieldgroup->build());
    });
}