<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$doorways = new FieldsBuilder('doorways');
$doorways
    ->addTab('Section settings')
    ->addSelect('background_color', [
        'label' => 'Background color',
        'choices' => [
            'white',
            'blue'
        ],
        'default_value' => ['blue'],
    ])
    ->addText('title')
    ->addWysiwyg('content')
    ->addTab('links')
    ->addRepeater('links')
        ->addLink('link')
    ->endRepeater()
    ->addTab('Section image')
        ->addImage('section_image');

return $doorways;