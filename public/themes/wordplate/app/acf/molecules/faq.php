<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$faq = new FieldsBuilder('faq');
$faq
    ->addTab('Section settings')
    ->addText('id', [
        'label' => 'section ID',
        'placeholder' => 'naam-van-sectie-zonder-spaties',
        'wrapper' => [
            'width' => '30%'
        ]
        ])
    ->addSelect('background_color', [
        'label' => 'Background color',
        'choices' => [
            'white',
            'blue'
        ],
        'default_value' => ['blue'],
        'wrapper' => [
            'width' => '70%'
        ]
    ])
    ->addText('title')
        ->addWysiwyg('content')
    ->addTab('questions')
        ->addRepeater('faqs')
            ->addText('question')
            ->addTextarea('answer')
        ->endRepeater()
    ->addTab('Section image')
        ->addImage('section_image');

return $faq;