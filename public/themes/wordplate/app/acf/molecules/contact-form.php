<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$contactForm = new FieldsBuilder('contact_form');
$contactForm
    ->addText('title')
    ->addWysiwyg('content');

return $contactForm;