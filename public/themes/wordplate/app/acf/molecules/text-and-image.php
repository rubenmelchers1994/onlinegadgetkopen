<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$textAndImage = new FieldsBuilder('text_and_image');
$textAndImage
    ->addText('title')
    ->addWysiwyg('content');

return $textAndImage;