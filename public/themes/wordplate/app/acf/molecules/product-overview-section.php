<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$productOverview = new FieldsBuilder('product_overview_section');
$productOverview
    ->addTab('Section settings')
        ->addText('title')
        ->addWysiwyg('content')
        ->addLink('button')
    ->addTab('Product picker')
        ->addRelationship('products', [
            'label' => 'Page Links',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['product'],
            'taxonomy' => [],
            'filters' => [
                0 => 'search',
                2 => 'taxonomy',
            ],
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'object',
        ]);

return $productOverview;