<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$textblock = new FieldsBuilder('textblock');
$textblock
    ->addText('id', [
        'label' => 'section ID',
        'placeholder' => 'naam-van-sectie-zonder-spaties',
        'wrapper' => [
            'width' => '30%'
        ]
        ])
    ->addText('title', [
        'wrapper' => [
            'width' => '70%'
        ]
        ])
    ->addWysiwyg('content');

return $textblock;