<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$table = new FieldsBuilder('table');
$table
    ->addText('title')
    ->addWysiwyg('content')
    ->addRepeater('table_rows')
        ->addText('label')
        ->addText('value')
    ->endRepeater();

return $table;