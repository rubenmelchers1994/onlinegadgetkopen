<?php

function fwp_wrapper_open() {
    if ( ! is_singular() ) : echo '<div class="facetwp-template">'; endif;
}
function fwp_wrapper_close() {
    if ( ! is_singular() ) : echo '</div><!-- end facetwp-template -->'; endif;
}
add_action( 'woocommerce_before_shop_loop', 'fwp_wrapper_open', 5 );
add_action( 'woocommerce_after_shop_loop', 'fwp_wrapper_close', 15 );
add_action( 'woocommerce_no_products_found', 'fwp_wrapper_open', 5 );
add_action( 'woocommerce_no_products_found', 'fwp_wrapper_close', 15 );

add_shortcode( 'demo_shortcode', 'demo_shortcode_callback_function' );

function demo_shortcode_callback_function( $atts ){
	return "Welcome to WordPress";
}