<?php

function navigation_menu( $theme_location ) {
    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
        // $menu = get_term( $locations[$theme_location], 'nav_menu' );
        // $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu = wp_get_nav_menu_object( $locations[ 'navigation' ] );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
    
 
        // $menu_list  = '<nav>' ."\n";
        $menu_list = '<ul class="main-nav list-unstyled d-flex mb-0 h-100 gap-3 justify-content-md-end align-items-md-center navigation__list" data-js-bind="menu-wrapper"> ' ."\n";
 
        $count = 0;
        $submenu = false;
         
        foreach( $menu_items as $menu_item ) {
             
            $link = $menu_item->url;
            $title = $menu_item->title;
             
            if ( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;
                 
                $menu_list .= '<li class="item navigation__listItem position-relative">' ."\n";
                $menu_list .= '<a href="'.$link.'" class="navigation__link text-decoration-none">' ."\n";
                $menu_list .= '<span class="navigation__label">'.$title.'</span>'."\n";
                $menu_list .= '</a>'."\n";
            }
 
            if ( $parent_id == $menu_item->menu_item_parent ) {
 
                if ( !$submenu ) {
                    $submenu = true;
                    $menu_list .= '<ul class="navigation__sub sub-menu list-unstyled d-flex flex-column">' ."\n";
                }
 
                $menu_list .= '<li class="item navigation__listItem">' ."\n";
                // $menu_list .= '<a href="'.$link.'" class="title navigation__link text-decoration-none">'.$title.'</a>' ."\n";
                $menu_list .= '<a href="'.$link.'" class="navigation__link navigation__link--sub text-decoration-none d-flex justify-content-between">' ."\n";
                $menu_list .= '<span class="navigation__label navigation__label--sub">'.$title.'</span>'."\n";
                $menu_list .= '<img src="'.get_template_directory_uri().'/assets/img/chevron-right-white.svg" alt="'.$title.'" class="ms-3">';
                $menu_list .= '</a>'."\n";
                $menu_list .= '</li>' ."\n";
                     
 
                if ( isset($menu_items[ $count + 1 ]) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
                    $menu_list .= '</ul>' ."\n";
                    $submenu = false;
                }
 
            }
 
            if ( isset($menu_items[ $count + 1 ]) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) { 
                $menu_list .= '</li>' ."\n";      
                $submenu = false;
            }
 
            $count++;
        }
         
        $menu_list .= '</ul>' ."\n";
        // $menu_list .= '</nav>' ."\n";
 
    } else {
        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
    }
    return $menu_list;
}