<?php 
  $logo = get_field('site_logo', 'options');
  $slogan = get_field('slogan', 'options');
  $page_links = get_field('page_links', 'options');
  $secondary_page_links = get_field('secondary_page_links', 'options');
?>

<footer class="footer">

    <div class="container">
      <div class="row">
        <div class="col-12 col-md-5 col-lg-3">
          <div class="footer__logo">
            <img src="<?= $logo['url'] ?>" alt="onlinegadgetkopen" class="img-fluid">
          </div>
          <h2 class="footer__slogan">
            <?= $slogan ?>
          </h2> 
        </div>
        <?php if($page_links) : ?>
          <div class="col-12 col-md-6 col-lg-3 offset-md-1 mt-3 mt-md-0">
            <ul class="footer__list list-unstyled mb-0 d-flex flex-column align-items-md-end">
              <?php foreach($page_links as $link): ?>
                <li class="doorways__listItem d-flex">
                    <a href="<?= get_permalink($link) ?>" class="doorways__link d-flex justify-content-between">
                        <span class="doorways__label">
                            <?= esc_html($link->post_title) ?>
                        </span>    
                        <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-right-white.svg" alt="<?= esc_html($link->post_title) ?>" class="doorways__icon">
                    </a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>

        <?php if($secondary_page_links) : ?>
          <div class="col-12 col-lg-3 offset-lg-1">
            <ul class="footer__list list-unstyled mb-0 d-flex flex-column align-items-md-end">
              <?php foreach($secondary_page_links as $link): ?>
                <li class="doorways__listItem d-flex">
                    <a href="<?= get_permalink($link) ?>" class="doorways__link d-flex justify-content-between">
                        <span class="doorways__label">
                            <?= esc_html($link->post_title) ?>
                        </span>    
                        <img src="<?= get_template_directory_uri() ?>/assets/img/chevron-right-white.svg" alt="<?= esc_html($link->post_title) ?>" class="doorways__icon">
                    </a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="footer__bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="footer__small">
              © <?= date('Y') ?> <a href="<?= home_url() ?>">onlinegadgetkopen.nl</a>. All rights reserved
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <?php wp_footer(); ?>
</body>
</html>
