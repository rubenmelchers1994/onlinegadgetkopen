const mix = require('laravel-mix');
const path = require('path');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');

require('dotenv').config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your WordPlate applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JavaScript files.
 |
 */

const theme = process.env.WP_DEFAULT_THEME;
const _package = require('./package.json');

mix.setResourceRoot('../');
mix.setPublicPath(`public/themes/${theme}`);
// mix.setPublicPath(path.resolve('./'))

mix.js('resources/scripts/app.js', `assets/js/app.js`)
  .extract(['jQuery']);

mix.sass('resources/styles/app.scss', `assets/css/app.css`)
  .options({
    autoprefixer: {
      options: {
          browsers: [
              'last 6 versions',
              ">0.25%",
              "not ie 11",
              "not op_mini all"
          ]
      }
    },
  });

mix.sass('resources/styles/header.scss', `style.css`, {
  additionalData: `$versiontag: '${_package.version}';`
});


mix.copyDirectory('resources/images', `public/themes/${theme}/assets/img`);
// mix.copy('mix-manifest.json', 'assets/mix-manifest.json');

if (!mix.inProduction() && process.env.SITE_URL && process.env.SITE_URL != '') {
  mix.browserSync(process.env.SITE_URL)
}

if (mix.inProduction()) {
  mix.version();
}
