import noUiSlider from 'nouislider';
import 'nouislider/dist/nouislider.css';

export default {
    init() {
      // JavaScript to be fired on the phone overview page
        const section = document.querySelector('[data-js-bind="phone-overview"]');
        if(!section) {
            return;
        }
        const phoneElements = section.querySelectorAll('[data-js-bind="phone"]');

        if(!phoneElements || phoneElements.length <= 0) {
            return;
        }

        const mobileFilterTrigger = section.querySelector('[data-js-bind="mobile-filter-trigger"]');
        const filterWrapper = section.querySelector('[data-js-bind="filter-outer"]');
        const closeButton = section.querySelector('[data-js-bind="mobile-menu-close-button"]');

        const brandFilterButtons = section.querySelectorAll('[data-js-bind="brand-filter-button"]');
        const sliderElement = section.querySelector('[data-js-bind="slider-element"]');
        const minCostEl = section.querySelector('[data-js-bind="min-cost"]');
        const maxCostEl = section.querySelector('[data-js-bind="max-cost"]');

        const minValue = [...phoneElements].reduce((prev, curr) => parseFloat(prev.dataset.price) < parseFloat(curr.dataset.price) ? prev : curr).dataset.price;
        const maxValue = [...phoneElements].reduce((prev, curr) => parseFloat(prev.dataset.price) > parseFloat(curr.dataset.price) ? prev : curr).dataset.price;
        let brands = [];

        let newMin = minValue;
        let newMax = maxValue;

        const filterPhones = () => {
            phoneElements.forEach(phone => {
                if(brands.length > 0 && !brands.includes(phone.dataset.brand)) {
                    phone.classList.add('d-none');
                }
                if(brands.length > 0 && brands.includes(phone.dataset.brand)) {
                    phone.classList.remove('d-none');
                }
                if(brands.length <= 0) {
                    phone.classList.remove('d-none');
                }

                if(parseFloat(phone.dataset.price) < newMin) {
                    phone.classList.add('d-none');
                }

                if(parseFloat(phone.dataset.price) > newMax) {
                    phone.classList.add('d-none');
                }
            })
        }

        const handleBrandClick = (button) => {
            const brand = button.dataset.brand;
            if(brands.includes(`${brand}`)) {
                //remove from array
                brands = brands.filter(item => item !== brand);
                button.classList.remove('phones__brand--active');
            } else {
                //add to array
                brands.push(brand);
                button.classList.add('phones__brand--active');
            }
            filterPhones();
        }

        brandFilterButtons.forEach(button => {
            button.addEventListener('click', () => {
                handleBrandClick(button);
            })
        })

        noUiSlider.create(sliderElement, {
            start: [parseInt(minValue), parseInt(maxValue)],
            connect: true,
            range: {
                'min': parseInt(minValue),
                'max': parseInt(maxValue)
            }
        });

        sliderElement.noUiSlider.on('update', function (values, handle) {
            console.log(values, handle);
            newMin = values[0];
            newMax = values[1];

            minCostEl.innerText = newMin;
            maxCostEl.innerText = newMax;

            filterPhones();
        });
        minCostEl.innerText = minValue;
        maxCostEl.innerText = maxValue;

        mobileFilterTrigger.addEventListener('click', () => {
            filterWrapper.classList.toggle('phones__filterOuter--show');
        })

        closeButton.addEventListener('click', () => {
            filterWrapper.classList.remove('phones__filterOuter--show');
        })

    },
    finalize() {
      // JavaScript to be fired on the page, after the init JS
    },
};
