// import Splide from '@splidejs/splide';
import svg from '../util/svg';
import anchor from '../util/anchor-link';
import anchorList from '../util/anchor-list';
import featuredSlider from '../util/featured-slider';
import navigation from '../util/navigation';
import filters from '../util/filters';

const handleSvgImages = () => {
  const svgImages = document.querySelectorAll('[data-js-bind="img-to-inline-svg"]');

  if(svgImages && svgImages.length > 0) {
    svgImages.forEach(image => {
      svg(image);
    })
  }
}

const handleAnchorLinks = () => {
  const anchors = document.querySelectorAll('[data-js-bind="anchor-link"]');

  if(anchors && anchors.length > 0) {
    anchors.forEach(anchorLink => {
      anchor(anchorLink);
    })
  }
}
const handleAnchorLists = () => {
  const anchorLists = document.querySelectorAll('[data-js-bind="anchor-list"]');

  if(anchorLists && anchorLists.length > 0) {
    anchorLists.forEach(list => {
      anchorList(list);
    })
  }
}

const handleFeaturedSliderElements = () => {
  const sliderElements = document.querySelectorAll('[data-js-bind="slider-element"]');

  if(sliderElements && sliderElements.length > 0) {
    sliderElements.forEach(sliderElement => {
      featuredSlider(sliderElement);
    })
  }
}

const handleNavigation = () => {
  const navigationEl = document.querySelector('[data-js-bind="navigation"]');

  if(navigationEl) {
    navigation(navigationEl);
  }
}

const handleFilterEl = () => {
  const filterEl = document.querySelector('[data-js-bind="filter-holder"]');
  if(filterEl) {
    filters(filterEl);
  }
}

export default {
  init() {
    // JavaScript to be fired on all pages
    handleNavigation();
    handleSvgImages();
    handleFeaturedSliderElements();
    handleFilterEl();
    // handleAnchorLists();
    // handleAnchorLinks();
    document.body.classList.add('loaded');
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
