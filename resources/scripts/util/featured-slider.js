import Glide from '@glidejs/glide'

export default sliderElement => {

  const sliderOptions = {
    type: 'carousel',
    // gaps: 15,
    perView: 3,
    rewind: true,
    perTouch: 1,
    peek: 0,
    breakpoints: {
      567: {
        perView: 1,
        peek: 45
      },
      768: {
        perView: 1,
        peek: 65
      },
      992: {
        perView: 2
      },
      1200: {
        perView: 3,
        peek: 45
      }
    }
  };

  new Glide(sliderElement, sliderOptions).mount();
}