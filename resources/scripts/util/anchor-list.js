export default anchorList => {
    const trigger = anchorList.querySelector('[data-js-bind="anchor-trigger"]');
    const wrapper = anchorList.querySelector('[data-js-bind="anchors-wrapper"]');
    const closeBtn = anchorList.querySelector('[data-js-bind="anchors-close"]');

    const toggleWrapper = () => {
        wrapper.classList.toggle('show');
    }

    trigger.addEventListener('click', toggleWrapper);
    closeBtn.addEventListener('click', toggleWrapper);
}