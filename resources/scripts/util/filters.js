export default filterHolder => {

    const trigger = filterHolder.querySelector('[data-js-bind="filters-trigger"]');
    const closeButton = filterHolder.querySelector('[data-js-bind="filters-close-button"]');

    trigger.addEventListener('click', () => {
        filterHolder.classList.add('overview__filtersHolder--show');
    })
    
    closeButton.addEventListener('click', () => {
        console.log('close');
        filterHolder.classList.remove('overview__filtersHolder--show');
    })
}