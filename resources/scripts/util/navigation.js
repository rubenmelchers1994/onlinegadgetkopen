export default navigation => {

  const menu = navigation.querySelector('[data-js-bind="menu-wrapper"]');
  const hamburger = navigation.querySelector('[data-js-bind="hamburger"]');
  let timeout;


  const toggleMenu = () => {
    hamburger.classList.add('navigation__hamburger--animating')
    menu.classList.toggle('navigation__sub--show')
    navigation.classList.toggle('navigation--toggled');
    setTimeout(() => {
      hamburger.classList.remove('navigation__hamburger--animating')
      hamburger.classList.toggle('navigation__hamburger--toggled')
    }, 500)
  }

  hamburger.addEventListener('click', () => {
    document.body.style.setProperty('--nav-height', `${navigation.offsetHeight}px`);
    menu.style.setProperty('--nav-height', `${navigation.offsetHeight}px`);
    setTimeout(() => {
      toggleMenu();
    }, 1)
  })
  document.body.style.setProperty('--nav-height', `${navigation.offsetHeight}px`);

}

(() => {
  'use strict';

  // const navigation = document.querySelector('[data-js-bind="navigation"]');
  
  // if(!navigation) {
  //   return;
  // }

  // const menu = navigation.querySelector('[data-js-bind="menu-wrapper"]');
  // const hamburger = navigation.querySelector('[data-js-bind="hamburger"]');
  // let timeout;

  // const handleLayout = () => {
  //   if(window.scrollY >= 100) {
  //     navigation.classList.add('navigation--scrolled');
  //   } else {
  //     navigation.classList.remove('navigation--scrolled');
  //   }
  // }

  // const toggleMenu = () => {
  //   hamburger.classList.add('navigation__hamburger--animating')
  //   menu.classList.toggle('navigation__menuWrapper--show')
  //   setTimeout(() => {
  //     hamburger.classList.remove('navigation__hamburger--animating')
  //     hamburger.classList.toggle('navigation__hamburger--toggled')
  //   }, 500)
  // }

  // window.addEventListener('scroll', () => {
  //   clearTimeout(timeout);
  //   setTimeout(() => {
  //     handleLayout();
  //   }, 20);
  // })

  // hamburger.addEventListener('click', () => {
  //   document.body.style.setProperty('--nav-height', `${navigation.offsetHeight}px`);
  //   menu.style.setProperty('--nav-height', `${navigation.offsetHeight}px`);
  //   setTimeout(() => {
  //     toggleMenu();
  //   }, 1)
  // })
  // document.body.style.setProperty('--nav-height', `${navigation.offsetHeight}px`);
  // handleLayout();
})();