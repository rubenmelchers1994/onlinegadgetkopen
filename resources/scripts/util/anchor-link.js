export default anchor => {
    const href = anchor.href;
    const offset = document.body.style.getPropertyValue('--nav-height');
    const destination = document.getElementById(href.split('#')[1])
    const scrollElement = document.createElement('span');

    anchor.addEventListener('click', e => {
        e.preventDefault();
        destination.appendChild(scrollElement);
        scrollElement.style.top = `calc(-${offset} - 40px)`;
        scrollElement.style.position = 'relative';
        setTimeout(() => {
            scrollElement.scrollIntoView({
                behavior: "smooth", 
                block: "start", 
                inline: "nearest"
            });
        }, 100)

        setTimeout(() => {
            // destination.removeChild(scrollElement);
        }, 500);
    })
}